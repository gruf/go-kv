//go:build kvformat
// +build kvformat

package kv_test

import (
	"codeberg.org/gruf/go-kv"
)

var fieldTests = []struct {
	field kv.Field
	vbose bool
	key   string
	value string
}{
	{
		field: kv.Field{
			K: `hello`,
			V: `world`,
		},
		key:   `hello`,
		value: `world`,
	},
	{
		field: kv.Field{
			K: `hello world`,
			V: `hello world`,
		},
		key:   `"hello world"`,
		value: `"hello world"`,
	},
	{
		field: kv.Field{
			K: "hello\tworld",
			V: "hello\tworld",
		},
		key:   "\"hello\tworld\"",
		value: "\"hello\tworld\"",
	},
	{
		field: kv.Field{
			K: "hello\tworld\r\n",
			V: "hello\tworld\r\n",
		},
		key:   `"hello\tworld\r\n"`,
		value: `"hello\tworld\r\n"`,
	},
	{
		field: kv.Field{
			K: `integer`,
			V: 69,
		},
		key:   `integer`,
		value: `69`,
	},
	{
		field: kv.Field{
			K: `integer`,
			V: -69,
		},
		key:   `integer`,
		value: `-69`,
	},
	{
		field: kv.Field{
			K: `float`,
			V: 420.69,
		},
		key:   `float`,
		value: `420.69`,
	},
	{
		field: kv.Field{
			K: `float`,
			V: -420.69,
		},
		key:   `float`,
		value: `-420.69`,
	},
	{
		field: kv.Field{
			K: `struct A`,
			V: structA{},
		},
		key:   `"struct A"`,
		value: `{}`,
	},
	{
		field: kv.Field{
			K: `struct A`,
			V: structA{},
		},
		vbose: true,
		key:   `"struct A"`,
		value: `kv_test.structA{}`,
	},
	{
		field: kv.Field{
			K: `struct B`,
			V: structB{},
		},
		key:   `"struct B"`,
		value: `{Field="" field=""}`,
	},
	{
		field: kv.Field{
			K: `struct B`,
			V: structB{
				Field: "hello world",
				field: "hello\tworld",
			},
		},
		key:   `"struct B"`,
		value: "{Field=\"hello world\" field=\"hello\tworld\"}",
	},
	{
		field: kv.Field{
			K: `struct B`,
			V: structB{
				Field: "hello\rworld",
				field: "hello\nworld",
			},
		},
		key:   `"struct B"`,
		value: `{Field="hello\rworld" field="hello\nworld"}`,
	},
	{
		field: kv.Field{
			K: `struct B`,
			V: structB{},
		},
		vbose: true,
		key:   `"struct B"`,
		value: `kv_test.structB{Field="" field=""}`,
	},
	{
		field: kv.Field{
			K: `struct B`,
			V: structB{
				Field: "hello world",
				field: "hello\tworld",
			},
		},
		vbose: true,
		key:   `"struct B"`,
		value: "kv_test.structB{Field=\"hello world\" field=\"hello\tworld\"}",
	},
	{
		field: kv.Field{
			K: `struct B`,
			V: structB{
				Field: "hello\rworld",
				field: "hello\nworld",
			},
		},
		vbose: true,
		key:   `"struct B"`,
		value: `kv_test.structB{Field="hello\rworld" field="hello\nworld"}`,
	},
	{
		field: kv.Field{
			K: `struct C`,
			V: structC{},
		},
		key:   `"struct C"`,
		value: `{Field='\x00' field=0}`,
	},
	{
		field: kv.Field{
			K: `struct C`,
			V: structC{},
		},
		vbose: true,
		key:   `"struct C"`,
		value: `kv_test.structC{Field='\x00' field=0}`,
	},
	{
		field: kv.Field{
			K: `struct D`,
			V: structD{},
		},
		key:   `"struct D"`,
		value: `"structD{}.String()"`,
	},
	{
		field: kv.Field{
			K: `struct D`,
			V: structD{},
		},
		vbose: true,
		key:   `"struct D"`,
		value: `kv_test.structD{}`,
	},
	{
		field: kv.Field{
			K: `struct E`,
			V: structE{},
		},
		key:   `"struct E"`,
		value: `{iface=nil ptr=nil uptr=nil}`,
	},
	{
		field: kv.Field{
			K: `struct E`,
			V: structE{},
		},
		vbose: true,
		key:   `"struct E"`,
		value: `kv_test.structE{iface=(interface {})(nil) ptr=(unsafe.Pointer)(nil) uptr=(uintptr)(nil)}`,
	},
	{
		field: kv.Field{
			K: `interface`,
			V: (interface{})(nil),
		},
		key:   `interface`,
		value: `nil`,
	},
	{
		field: kv.Field{
			K: `interface`,
			V: (interface{})(nil),
		},
		vbose: true,
		key:   `interface`,
		value: `nil`,
	},
	{
		field: kv.Field{
			K: `interface`,
			V: (interface{})(69),
		},
		key:   `interface`,
		value: `69`,
	},
	{
		field: kv.Field{
			K: `interface`,
			V: (interface{})(69),
		},
		vbose: true,
		key:   `interface`,
		value: `69`,
	},
	{
		field: kv.Field{
			K: `interface`,
			V: (interface{})("hello world"),
		},
		key:   `interface`,
		value: `"hello world"`,
	},
	{
		field: kv.Field{
			K: `interface`,
			V: (interface{})("hello world"),
		},
		vbose: true,
		key:   `interface`,
		value: `"hello world"`,
	},
	{
		field: kv.Field{
			K: `msg`,
			V: `error fetching account featured collection: Dereference: GET request to https://example.com/users/foobar/collections/featured failed: status=\"403 Forbidden\" body=\"{\"error\":\"Forbidden\"}\"`,
		},
		vbose: true,
		key:   `msg`,
		value: `"error fetching account featured collection: Dereference: GET request to https://example.com/users/foobar/collections/featured failed: status=\\\"403 Forbidden\\\" body=\\\"{\\\"error\\\":\\\"Forbidden\\\"}\\\""`,
	},
}
