package format_test

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"testing"
	"time"
	"unsafe"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv/format"
)

// A is a test structure.
type A struct {
	A string
	B *string
}

// B is a test structure with unexported fields.
type B struct {
	a string
	b *string
}

// C is a test structure with fmt.Stringer implementation.
type C struct{}

func (c C) String() string {
	return "c.String()"
}

type D struct {
	b []byte
}

// PanicTest is a test structure with a fmt.Stringer implementation that panics.
type PanicTest int

func (t PanicTest) String() string {
	panic(`oh no! a panic`)
}

// appendTests are just a list of arguments to
// format and append to buffer, these test results
// are not checked these are to ensure safety.
var appendTests = []interface{}{}

// printfTests provide a list of format strings, with
// single argument and expected results. Intended to
// test that operands produce expected results.
var printfTests = []struct {
	Fmt string
	Arg interface{}
	Out string
}{
	// default format
	{
		Fmt: `{}`,
		Arg: nil,
		Out: `nil`,
	},
	{
		Fmt: `{}`,
		Arg: (*int)(nil),
		Out: `nil`,
	},
	{
		Fmt: `{}`,
		Arg: time.Second,
		Out: time.Second.String(),
	},
	{
		Fmt: `{}`,
		Arg: `hello`,
		Out: `hello`,
	},
	{
		Fmt: `{}`,
		Arg: `hello world`,
		Out: `hello world`,
	},
	{
		Fmt: `{}`,
		Arg: "hello\nworld\n",
		Out: "hello\nworld\n",
	},
	{
		Fmt: `{}`,
		Arg: func() interface{} { s := "hello\nworld\n"; return &s }(),
		Out: "hello\nworld\n",
	},
	{
		Fmt: `{}`,
		Arg: errors.New("error!"),
		Out: `error!`,
	},
	{
		Fmt: `{}`,
		Arg: A{},
		Out: `{A="" B=nil}`,
	},
	{
		Fmt: `{}`,
		Arg: B{},
		Out: `{a="" b=nil}`,
	},
	{
		Fmt: `{}`,
		Arg: C{},
		Out: `c.String()`,
	},
	{
		Fmt: `{}`,
		Arg: PanicTest(0),
		Out: `!{PANIC="oh no! a panic"}`,
	},
	{
		Fmt: `{}`,
		Arg: int8(0),
		Out: `0`,
	},
	{
		Fmt: `{}`,
		Arg: int16(0),
		Out: `0`,
	},
	{
		Fmt: `{}`,
		Arg: int32(0),
		Out: string(rune(0)),
	},
	{
		Fmt: `{}`,
		Arg: int64(0),
		Out: `0`,
	},
	{
		Fmt: `{}`,
		Arg: uint8(0),        // uint8=byte
		Out: string(byte(0)), // formatted as byte
	},
	{
		Fmt: `{}`,
		Arg: uint16(0),
		Out: `0`,
	},
	{
		Fmt: `{}`,
		Arg: uint32(0),
		Out: `0`,
	},
	{
		Fmt: `{}`,
		Arg: uint64(0),
		Out: `0`,
	},
	{
		Fmt: `{}`,
		Arg: float32(0.0),
		Out: `0`,
	},
	{
		Fmt: `{}`,
		Arg: float64(0.0),
		Out: `0`,
	},
	{
		Fmt: `{}`,
		Arg: complex64(0),
		Out: `0+0i`,
	},
	{
		Fmt: `{}`,
		Arg: complex128(0),
		Out: `0+0i`,
	},
	{
		Fmt: `{}`,
		Arg: []byte(`hello world`),
		Out: `hello world`,
	},
	{
		Fmt: `{}`,
		Arg: byte('?'),
		Out: `?`,
	},
	{
		Fmt: `{}`,
		Arg: byte('\n'),
		Out: "\n",
	},
	{
		Fmt: `{}`,
		Arg: func() interface{} { b := byte('\n'); return &b }(),
		Out: "\n",
	},
	{
		Fmt: `{}`,
		Arg: []int{0, 1, 2, 3, 4, 5},
		Out: `[0,1,2,3,4,5]`,
	},
	{
		Fmt: `{}`,
		Arg: []string{"hello !", "1", "2", "3", "\n"},
		Out: `["hello !","1","2","3","\n"]`,
	},
	{
		Fmt: `{}`,
		Arg: []([]byte){[]byte("hello !"), []byte("1"), []byte("2"), []byte("3"), []byte("\n")},
		Out: `["hello !","1","2","3","\n"]`,
	},
	{
		Fmt: `{}`,
		Arg: map[string]interface{}{
			"duration": time.Second,
			"string":   "aaa",
			"panic":    PanicTest(0),
			"submap":   map[string]interface{}{"1": 1, "2": 2, "3": 3},
		},
		Out: `{duration="1s" string="aaa" panic=!{PANIC="oh no! a panic"} submap={1=1 2=2 3=3}}`,
	},
	{
		Fmt: `{}`,
		Arg: D{b: []byte("hello world")},
		Out: `{b="hello world"}`,
	},

	// key format
	{
		Fmt: `{:k}`,
		Arg: nil,
		Out: `nil`,
	},
	{
		Fmt: `{:k}`,
		Arg: (*int)(nil),
		Out: `nil`,
	},
	{
		Fmt: `{:k}`,
		Arg: time.Second,
		Out: time.Second.String(),
	},
	{
		Fmt: `{:k}`,
		Arg: `hello`,
		Out: `hello`,
	},
	{
		Fmt: `{:k}`,
		Arg: `hello world`,
		Out: `"hello world"`,
	},
	{
		Fmt: `{:k}`,
		Arg: "hello\nworld\n",
		Out: `"hello\nworld\n"`,
	},
	{
		Fmt: `{:k}`,
		Arg: errors.New("error!"),
		Out: `error!`,
	},
	{
		Fmt: `{:k}`,
		Arg: A{},
		Out: `{A="" B=nil}`,
	},
	{
		Fmt: `{:k}`,
		Arg: B{},
		Out: `{a="" b=nil}`,
	},
	{
		Fmt: `{:k}`,
		Arg: C{},
		Out: `c.String()`,
	},
	{
		Fmt: `{:k}`,
		Arg: PanicTest(0),
		Out: `!{PANIC="oh no! a panic"}`,
	},
	{
		Fmt: `{:k}`,
		Arg: []byte(`hello world`),
		Out: `"hello world"`,
	},
	{
		Fmt: `{:k}`,
		Arg: byte('?'),
		Out: `'?'`,
	},
	{
		Fmt: `{:k}`,
		Arg: byte('\n'),
		Out: `'\n'`,
	},

	// value format
	{
		Fmt: `{:v}`,
		Arg: nil,
		Out: `nil`,
	},
	{
		Fmt: `{:v}`,
		Arg: (*int)(nil),
		Out: `nil`,
	},
	{
		Fmt: `{:v}`,
		Arg: time.Second,
		Out: `"` + time.Second.String() + `"`,
	},
	{
		Fmt: `{:v}`,
		Arg: `hello`,
		Out: `"hello"`,
	},
	{
		Fmt: `{:v}`,
		Arg: `hello world`,
		Out: `"hello world"`,
	},
	{
		Fmt: `{:v}`,
		Arg: "hello\nworld\n",
		Out: `"hello\nworld\n"`,
	},
	{
		Fmt: `{:v}`,
		Arg: errors.New("error!"),
		Out: `"error!"`,
	},
	{
		Fmt: `{:v}`,
		Arg: A{},
		Out: `{A="" B=nil}`,
	},
	{
		Fmt: `{:v}`,
		Arg: B{},
		Out: `{a="" b=nil}`,
	},
	{
		Fmt: `{:v}`,
		Arg: C{},
		Out: `"c.String()"`,
	},
	{
		Fmt: `{:v}`,
		Arg: PanicTest(0),
		Out: `!{PANIC="oh no! a panic"}`,
	},
	{
		Fmt: `{:v}`,
		Arg: []byte(`hello world`),
		Out: `"hello world"`,
	},
	{
		Fmt: `{:v}`,
		Arg: byte('?'),
		Out: `'?'`,
	},
	{
		Fmt: `{:v}`,
		Arg: byte('\n'),
		Out: `'\n'`,
	},

	// verbose format
	{
		Fmt: `{:?}`,
		Arg: nil,
		Out: `nil`,
	},
	{
		Fmt: `{:?}`,
		Arg: (*int)(nil),
		Out: `(*int)(nil)`,
	},
	{
		Fmt: `{:?}`,
		Arg: time.Second,
		Out: strconv.FormatInt(int64(time.Second), 10),
	},
	{
		Fmt: `{:?}`,
		Arg: `hello`,
		Out: `"hello"`,
	},
	{
		Fmt: `{:?}`,
		Arg: `hello world`,
		Out: `"hello world"`,
	},
	{
		Fmt: `{:?}`,
		Arg: "hello\nworld\n",
		Out: `"hello\nworld\n"`,
	},
	{
		Fmt: `{:?}`,
		Arg: func() interface{} { s := "hello\nworld\n"; return &s }(),
		Out: `(*string)("hello\nworld\n")`,
	},
	{
		Fmt: `{:?}`,
		Arg: errors.New("error!"), //nolint
		Out: `*errors.errorString{s="error!"}`,
	},
	{
		Fmt: `{:?}`,
		Arg: A{},
		Out: `format_test.A{A="" B=(*string)(nil)}`,
	},
	{
		Fmt: `{:?}`,
		Arg: B{},
		Out: `format_test.B{a="" b=(*string)(nil)}`,
	},
	{
		Fmt: `{:?}`,
		Arg: C{},
		Out: `format_test.C{}`,
	},
	{
		Fmt: `{:?}`,
		Arg: PanicTest(0),
		Out: `0`,
	},
	{
		Fmt: `{:?}`,
		Arg: []byte(`hello world`),
		Out: `[]uint8{'h','e','l','l','o',' ','w','o','r','l','d'}`,
	},
	{
		Fmt: `{:?}`,
		Arg: byte('?'),
		Out: `'?'`,
	},
	{
		Fmt: `{:?}`,
		Arg: byte('\n'),
		Out: `'\n'`,
	},
	{
		Fmt: `{:?}`,
		Arg: func() interface{} { b := byte('\n'); return &b }(),
		Out: `(*uint8)('\n')`,
	},
	{
		Fmt: `{:?}`,
		Arg: []int{0, 1, 2, 3, 4, 5},
		Out: `[]int{0,1,2,3,4,5}`,
	},
	{
		Fmt: `{:?}`,
		Arg: []string{"hello !", "1", "2", "3", "\n"},
		Out: `[]string{"hello !","1","2","3","\n"}`,
	},
	{
		Fmt: `{:?}`,
		Arg: []([]byte){[]byte("hello !"), []byte("1"), []byte("2"), []byte("3"), []byte("\n")},
		Out: `[][]uint8{[]uint8{'h','e','l','l','o',' ','!'},[]uint8{'1'},[]uint8{'2'},[]uint8{'3'},[]uint8{'\n'}}`,
	},
	{
		Fmt: `{:?}`,
		Arg: map[string]interface{}{
			"duration": time.Second,
			"string":   "aaa",
			"panic":    PanicTest(0),
			"submap":   map[string]interface{}{"1": 1, "2": 2, "3": 3},
		},
		Out: `map[string]interface {}{duration=1000000000 string="aaa" panic=0 submap=map[string]interface {}{1=1 2=2 3=3}}`,
	},
	{
		Fmt: `{:?}`,
		Arg: D{b: []byte("hello world")},
		Out: `format_test.D{b=[]uint8{'h','e','l','l','o',' ','w','o','r','l','d'}}`,
	},
}

// printfMultiTests provide a list of more complex format
// strings, with any number of arguments and expected results.
// Intended to test that string format parsing and formatting
// produces expected results.
var printfMultiTests = []struct {
	Fmt string
	Arg []interface{}
	Out string
}{}

var fmtTests = []interface{}{
	"hello world",
	http.Server{},
	sync.Mutex{},
	sync.WaitGroup{},
	(*url.URL)(nil),
	http.Request{},
	0,
	1,
	2,
	3,
	4,
	5,
	time.Second * 30,
	map[string]string{
		"hello": "world",
		"key":   "value",
	},
	[]int{10, 11, 12, 13, 14, 15},
	[]interface{}{"hello", "world", 12.2, 12, []byte("bytes")},
	[]string{"hello", "world", "string", "array"},
	[]byte("hello world this is a byte slice"),
	[]rune("hello world this is a rune slice"),
	',',
	420.69,
	time.Time{},
	A{},
	B{},
	C{},
	PanicTest(0),
	uintptr(0),
	unsafe.Pointer(nil),
}

func TestAppend(t *testing.T) {
	buf := byteutil.Buffer{make([]byte, 0, 1024)}
	for _, arg := range appendTests {
		format.Append(&buf, arg)
		buf.Reset()
	}
}

func TestAppendf(t *testing.T) {
	buf := byteutil.Buffer{make([]byte, 0, 1024)}
	for _, test := range printfTests {
		format.Appendf(&buf, test.Fmt, test.Arg)
		if buf.String() != test.Out {
			t.Errorf("printf did not produce expected results\n"+
				"input={%q, %#v}\n"+
				"expect=%s\n"+
				"actual=%s\n",
				test.Fmt,
				test.Arg,
				test.Out,
				buf.String(),
			)
		}
		buf.Reset()
	}
}

func TestAppendfMulti(t *testing.T) {
	buf := byteutil.Buffer{make([]byte, 0, 1024)}
	for _, test := range printfMultiTests {
		format.Appendf(&buf, test.Fmt, test.Arg...)
		if buf.String() != test.Out {
			t.Errorf("printf (multi arg) did not produce expected results\n"+
				"input={%q, %#v}\n"+
				"expect=%s\n"+
				"actual=%s\n",
				test.Fmt,
				test.Arg,
				test.Out,
				buf.String(),
			)
		}
		buf.Reset()
	}
}

func BenchmarkFprintf(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		buf := byteutil.Buffer{make([]byte, 0, 1024)}
		for pb.Next() {
			for i := range fmtTests {
				// Perform both non-verbose and verbose
				format.Appendf(&buf, "{}", fmtTests[i])
				buf.Reset()
				format.Appendf(&buf, "{:?}", fmtTests[i])
				buf.Reset()
			}
		}
	})
}

func BenchmarkFmtFprintf(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			for i := range fmtTests {
				// Perform both non-verbose and verbose
				fmt.Fprintf(io.Discard, "%+v", fmtTests[i])
				fmt.Fprintf(io.Discard, "%#v", fmtTests[i])
			}
		}
	})
}
