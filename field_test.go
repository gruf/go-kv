package kv_test

import (
	"fmt"
	"net/http"
	"sync"
	"testing"
	"time"
	"unsafe"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
)

type structA struct{}

type structB struct {
	Field string
	field string
}

type structC struct {
	Field uint8
	field int8
}

type structD struct{}

func (structD) String() string {
	return "structD{}.String()"
}

func (structD) GoString() string {
	return "structD{}.GoString()"
}

type structE struct {
	iface interface{}
	ptr   unsafe.Pointer
	uptr  uintptr
}

var fields = kv.Fields{
	{K: "int", V: 1},
	{K: "float", V: 1.01},
	{K: "complex", V: 1.0 + 1.0i},
	{K: "duration", V: time.Second * 30},
	{K: "time", V: time.Now()},
	{K: "string", V: "hello world!!! the common test string"},
	{K: "bytes", V: []byte("hello world!!! but this time its bytes")},
	{K: "runes", V: []rune("hello world!!! but this time its runes??")},
	{K: "http client", V: http.Client{}},
	{K: "http server", V: http.Server{}},
	{K: "sync mutex", V: sync.Mutex{}},
	{K: "sync pool", V: sync.Pool{}},
	{K: "nil", V: nil},
	{K: "intptr", V: (*int)(nil)},
	{K: "floatptr", V: (*float64)(nil)},
	{K: "stringptr", V: (*string)(nil)},
	{K: "b", V: byte('b')},
}

func TestField(t *testing.T) {
	for _, test := range fieldTests {
		switch {
		// Check key formatting as expected
		case test.field.Key() != test.key:
			t.Errorf("formatted field key not as expected: expect=%s actual=%s", test.key, test.field.Key())

		// Check value formatting as expected
		case test.field.Value(test.vbose) != test.value:
			t.Errorf("formatted field value not as expected: expect=%s actual=%s", test.value, test.field.Value(test.vbose))

		// Check combined key-value formatting as expected (if non-verbose fails, try verbose)
		case test.field.String() != (test.key+"="+test.value) && test.field.GoString() != (test.key+"="+test.value):
			t.Errorf("formated field key=value not as expected: expect=%s actual=%s", (test.key + "=" + test.value), test.field.String())
		}
	}
}

func BenchmarkFieldAppendMulti(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		buf := byteutil.Buffer{B: make([]byte, 0, 4096)}
		for pb.Next() {
			for i := 0; i < len(fields); i++ {
				fields[i].AppendFormat(&buf, false)
				buf.Reset()
			}
		}
	})
}

func BenchmarkFieldStringMulti(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			for i := 0; i < len(fields); i++ {
				_ = fields[i].String()
			}
		}
	})
}

func BenchmarkFieldFprintfMulti(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		buf := byteutil.Buffer{B: make([]byte, 0, 4096)}
		for pb.Next() {
			for i := 0; i < len(fields); i++ {
				fmt.Fprintf(&buf, "%q=%+v", fields[i].K, fields[i].V)
				buf.Reset()
			}
		}
	})
}

func BenchmarkFieldsAppend(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		buf := byteutil.Buffer{B: make([]byte, 0, 4096)}
		for pb.Next() {
			kv.Fields(fields).AppendFormat(&buf, false)
			buf.Reset()
		}
	})
}

func BenchmarkFieldsString(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_ = kv.Fields(fields).String()
		}
	})
}
